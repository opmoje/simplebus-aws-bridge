<?php

namespace BNNVARA\SimpleBusAwsBridge\Queue;

use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\EmptyQueueException;
use BNNVARA\SimpleBusAwsBridge\Queue\Consumer\QueueConsumerInterface;

class QueueProcessor
{
    /** @var QueueConsumerInterface */
    private $queueConsumer;

    public function __construct(QueueConsumerInterface $queueConsumer)
    {
        $this->queueConsumer = $queueConsumer;
    }

    public function consume(QueueName $queueName): void
    {
        while($this->forever())
        {
            try {
                $this->queueConsumer->consume($queueName);
            } catch(EmptyQueueException $e)
            {
                sleep(1);
            }
        }
    }

    protected function forever(): bool
    {
        return true;
    }
}