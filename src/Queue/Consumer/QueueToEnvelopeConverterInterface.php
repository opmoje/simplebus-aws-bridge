<?php

namespace BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

interface QueueToEnvelopeConverterInterface
{
    /**
     * @param array $message
     *
     * @return MessageEnvelope
     */
    public function convert(array $message): MessageEnvelope;
}