<?php

namespace BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

use BNNVARA\SimpleBusAwsBridge\Queue\QueueName;
use Aws\Result;
use Aws\Sqs\SqsClient;
use SimpleBus\SymfonyBridge\Bus\EventBus;

class SqsConsumer implements QueueConsumerInterface
{
    /** @var SqsClient */
    private $client;

    /** @var string */
    private $endPoint;

    /** @var EventBus */
    private $eventBus;

    /** @var QueueToEnvelopeConverterInterface */
    private $messageToEnvelopeConverter;

    public function __construct(
        SqsClient $client,
        string $endPoint,
        QueueToEnvelopeConverterInterface $messageToEnvelopeConverter,
        EventBus $eventBus
    ) {
        $this->client = $client;
        $this->endPoint = $endPoint;
        $this->eventBus = $eventBus;
        $this->messageToEnvelopeConverter = $messageToEnvelopeConverter;
    }

    /** @inheritdoc */
    public function consume(QueueName $queue): void
    {
        $message = $this->getMessageFromQueue($queue);
        $envelope = $this->messageToEnvelopeConverter->convert($message);

        $this->eventBus->handle($envelope->getMessage());
        $this->acknowledgeMessage($queue, $envelope);
    }

    /**
     * @param QueueName $queue
     *
     * @throws EmptyQueueException
     * @return array
     */
    private function getMessageFromQueue(QueueName $queue): array
    {
        $messages = $this->client->receiveMessage(
            [
                'QueueUrl' => sprintf('%s%s', $this->endPoint, $queue),
                'VisibilityTimeout' => 10
            ]
        )->get('Messages');

        if ($messages === null) {
            throw new EmptyQueueException();
        }
        /** @var array $messages */
        /** @var array $message */
        $message = reset($messages);
        return $message;
    }

    private function acknowledgeMessage(QueueName $queue, MessageEnvelope $envelope): void
    {
        $this->client->deleteMessage([
            'QueueUrl' => sprintf('%s%s', $this->endPoint, $queue),
            'ReceiptHandle' => $envelope->getId()
        ]);
    }
}
