<?php

namespace BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

use JMS\Serializer\SerializerInterface;

class SqsToEnvelopeConverter implements QueueToEnvelopeConverterInterface
{
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function convert(array $message): MessageEnvelope
    {
        $messageObject = json_decode($message['Body']);
        $eventData = json_decode($messageObject->Message);
        $event = $this->serializer->deserialize($eventData->serialized_message, $eventData->message_type, 'json');

        return new MessageEnvelope($message['ReceiptHandle'], $event);
    }
}
