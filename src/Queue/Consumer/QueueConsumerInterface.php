<?php

namespace BNNVARA\SimpleBusAwsBridge\Queue\Consumer;

use BNNVARA\SimpleBusAwsBridge\Queue\QueueName;

interface QueueConsumerInterface
{
    /**
     * @param QueueName $queue
     */
    public function consume(QueueName $queue): void;
}